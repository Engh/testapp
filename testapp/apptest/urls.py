from django.conf.urls import patterns, include, url

urlpatterns = patterns('testapp.apptest.views',
    url(r'^$', 'home', name='home'),
    url(r'^reg/$', 'reg', name='reg'),
    url(r'^login/$', 'login', name='login'),
    url(r'^logout/$', 'logout', name='logout'),
    url(r'^profile/(?P<id>[?a-z=A-Z0-9_.-]*)/$', 'profile', name='profile'),
    url(r'^edit/(?P<id>[?a-z=A-Z0-9_.-]*)/$', 'edit', name='edit'),
)
