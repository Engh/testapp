from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib import auth

def home(request):
    if request.user:
        user = request.user
    temlate_date = {'user': user}
    return render_to_response('home/others/home.html', temlate_date)

def login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return HttpResponseRedirect('/')
        else:
            return render_to_response('home/others/login.html')
    else:
        return render_to_response('home/others/login.html')

def logout(request):
    auth.logout(request)
    return redirect('/')

def reg(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect('/')
    else:
        form = UserCreationForm()
    temlate_date = {'form': form}
    return render_to_response('home/others/reg.html', temlate_date)

def profile(request, id):
    if request.user:
        us = request.user
    user = User.objects.filter(id=id)
    template_data = {'user': user}
    return render_to_response('home/others/profile.html', template_data)

def edit(request, id):
    if request.user:
        us = request.user
    user = User.objects.filter(id=id)
    if request.POST:
        user = User.objects.get(pk = request.user.id)
        user.username = request.POST.get('username')
        user.first_name = request.POST.get('first_name')
        user.last_name = request.POST.get('last_name')
        user.email = request.POST.get('email')
        user.save()
        return HttpResponseRedirect('/')
    template_data = {'user' : user}
    return render_to_response('home/others/edit.html', template_data)
